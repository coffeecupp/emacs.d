;;; misc-settings --- coffeecupp
;;; Commentary:
;;; Misc settings for emacs
;;; Code:

;;; User information
(setq user-full-name "coffeecupp")
(setq user-mail-address "coffeecupp@ghostmail.com")

;;; Auto insert matching braces
(electric-pair-mode t)

;;; Show paren mode
(show-paren-mode t)

;;; Make yes and no y n
(fset 'yes-or-no-p 'y-or-n-p)

;;; No splash screen
(setq inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)

;;; No tabs
(setq tab-width 2
      indent-tabs-mode nil)

;;; No backup files
(setq make-backup-files nil)

;;; 80 chars only
(setq fill-column 80)

(provide 'misc-settings)
;;; misc-settings.el ends here
