;;; mode-settings --- coffeecupp
;;; Commentary:
;;; Mode settings for major and minor modes
;;; Code:

;;; show whitespace line
(add-hook 'prog-mode-hook #'whitespace-mode)

;;; Disable auto complete in python mode
(defadvice auto-complete-mode (around disable-auto-complete-for-python)
  "Disable auto complete in python mode."
  (unless (eq major-mode 'python-mode) ad-do-it))
(ad-activate 'auto-complete-mode)

;;; Python mode hooks
(add-hook
 'python-mode-hook
 '(lambda () "Python Mode hooks"
    (jedi:setup)
    (setq indent-tabs-mode nil)
    (setq tab-width 4)
    (setq python-indent-offset 4)
    (yas-minor-mode)
    (add-to-list 'company-backends 'company-jedi)))

;;; C++
(eval-after-load 'c++-mode
  '(lambda () "DOCSTRING"
     '(add-to-list 'company-c-headers-path-system "/usr/include/c++/5")
     '(add-to-list 'company-c-headers-path-system "/usr/include/x86_64-linux-gnu/c++/5")
     '(add-to-list 'company-c-headers-path-system "/usr/include/c++/5/backward")
     '(add-to-list 'company-c-headers-path-system "/usr/lib/gcc/x86_64-linux-gnu/5/include")
     '(add-to-list 'company-c-headers-path-system "/usr/local/include")
     '(add-to-list 'company-c-headers-path-system " /usr/lib/gcc/x86_64-linux-gnu/5/include-fixed")
     '(add-to-list 'company-c-headers-path-system "/usr/include/x86_64-linux-gnu")
     '(add-to-list 'company-c-headers-path-system "/usr/include")))

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-hook 'c++-mode-hook
	  '(lambda () "C++ Hooks"
	     (setq indent-tabs-mode nil
		   tab-width 4)
	     (setq c-default-style "linux"
		   c-basic-offset 4)
	     (yas-minor-mode)
	     (irony-mode)
	     (add-to-list 'company-backends 'company-irony)
	     (add-to-list 'company-backends 'company-c-headers)))

;;; C
(eval-after-load 'c-mode
  '(lambda () "DOCSTRING"
     '(add-to-list 'company-c-headers-path-system "/usr/lib/gcc/x86_64-linux-gnu/5/include")
     '(add-to-list 'company-c-headers-path-system "/usr/local/include")
     '(add-to-list 'company-c-headers-path-system "/usr/lib/gcc/x86_64-linux-gnu/5/include-fixed")
     '(add-to-list 'company-c-headers-path-system "/usr/include/x86_64-linux-gnu")
     '(add-to-list 'company-c-headers-path-system "/usr/include")))

(add-hook 'c-mode-hook
	  '(lambda () "C++ Hooks"
	     (setq indent-tabs-mode nil
		   tab-width 4)
	     (yas-minor-mode)
	     (setq c-default-style "linux"
		   c-basic-offset 4)
	     (irony-mode)
	     (add-to-list 'company-backends 'company-irony)
	     (add-to-list 'company-backends 'company-c-headers)))

;;; GO

;;; Ruby
(autoload 'enh-ruby-mode "enh-ruby-mode" "Major mode for ruby files" t)
(add-to-list 'auto-mode-alist '("\\.rb$" . enh-ruby-mode))
(add-to-list 'interpreter-mode-alist '("ruby" . enh-ruby-mode))

;;; Start inf-ruby
(autoload 'inf-ruby-minor-mode "inf-ruby" "Run an inferior Ruby process" t)
(add-hook 'enh-ruby-mode-hook 'inf-ruby-minor-mode)


(provide 'mode-settings)
;;; mode-settings.el ends here
