;;; appearance-settings --- coffeecupp
;;; Commentary:
;;; Basic appearance settings
;;; Code:

;;; Load path for themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes.d")

;;; Load theme
(load-theme 'badwolf t)

;;; Cursor color and type
(setq-default cursor-type 'bar)
(set-cursor-color "purple")

;;; Color column
(setq-default
 whitespace-line-column 80
 whitespace-style       '(face lines-tail))

;;; No blink
(if (fboundp 'blink-cursor-mode)
    (blink-cursor-mode -1))

;;; Show line numbers
(global-linum-mode 1)

;;; Column numbers
(setq column-number-mode t)

;;; No GUI it is just distracting
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; set a default font
(when (member "Envy Code R" (font-family-list))
  (set-face-attribute 'default nil :font "Envy Code R 11"))

;;; Set frame size
(setq initial-frame-alist
      '((width . 120)
        (height . 35)))

(setq default-frame-alist
      '((width . 120)
        (height . 35)))

(provide 'appearance-settings)
;;; appearance-settings ends here
